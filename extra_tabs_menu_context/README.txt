
Extra Tabs Menu Context
------------------------

    Provides a context reaction that allows a menu parent and its children to
    be displayed as extra tabs on a page.

Usage
------

    * Create a new context with whatever conditions you'd like
    * Add the "Extra Tabs Menu" reaction, and choose the menu parent whose
      links you'd like to appear as tabs when the conditions are met.
